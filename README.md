# spatman

Spatial organisation of human activities and their impact on biodiversity.

Working group CESAB 2023-2026


> Welcome!



# Participants

- Isabelle Boulangeat (PI)
- Cathleen Petit (postdoc)
- Isabelle Witté (data)
- Yves Schaeffer (member)
- Frédéric Gosselin (member)
- Karine Princé (invited)
- Laurent Bergès (core member)
- Mohamed Hilal (PI)

